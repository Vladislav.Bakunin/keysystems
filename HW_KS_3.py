class Geom_figure:
	#cоздал класс родителя,инициирую только имя
	def __init__(self, name):
		self.name = name
	def tell(self):
		print('Название фигуры: {},'.format(self.name),end=' ')
class Volumetric_figure(Geom_figure):
	#дочерний класс для родителя geom_figure
	#(объемные фигуры,только Куб и параллелепипед)
	def __init__(self,name,length,width,height):
		Geom_figure.__init__(self, name)
		self.length=length
		self.width=width
		self.height=height
	def tell(self):
		Geom_figure.tell(self)
		volume_of_figure=self.height*self.width*self.length
		perimetr_of_figure=(self.width+self.length)*2
		ploshad_of_figure=(self.length*self.width)
		print('Объем:{}, Периметр:{}, Площадь:{}'.format(volume_of_figure,perimetr_of_figure,ploshad_of_figure))
class Flat_figure(Geom_figure):
	def __init__(self,name,diameter):
		Geom_figure.__init__(self,name)
		self.diameter=diameter
		def tell(self):
			Geom_figure.tell(self)
			print()
class Flat_figure_with_top(Flat_figure):
	#Будет работать для квадрата
	def __init__(self,name,diameter,leanth):
		Flat_figure.__init__(self,name,diameter)
		self.leanth=leanth
	def tell(self):
		Flat_figure.tell(self)
		perimetr=self.leanth**2
		ploshad=(self.diameter**2)/2
		print('Площадь фигуры:{}, Периметр фигуры:{}'.format(ploshad,perimetr))
class Flat_figure_without_top(Flat_figure):
	def __init__(self,name,diameter,pi=3.14):
		Flat_figure.__init__(self,name,diameter)
		self.pi=pi
	def tell(self):
		Flat_figure.tell(self)
class Flat_figure_without_top_circle(Flat_figure_without_top):
	def __init__(self,name,diameter,radius,pi=3.14):
		Flat_figure_without_top.__init__(self,name,diameter,pi=3.14)
		self.radius=radius
	def tell(self):
		Flat_figure_without_top.tell(self)
		leanth_of_circle=2*self.pi*self.radius
		ploshad_of_circle=self.pi*(self.radius**2)
		print('Длина окружности:{}, Площадь окружности:{}'.format(leanth_of_circle,ploshad_of_circle))
class Flat_figure_without_top_ellips(Flat_figure_without_top):
        def __init__(self,name,diameter,diameter2,pi=3.14):
                Flat_figure_without_top.__init__(self,name,diameter,pi)
                self.diameter2=diameter2
        def tell(self):
                Flat_figure_without_top.tell(self)
                ploshad_of_ellips=self.pi*self.diameter2*self.diameter
                perimetr_of_ellips=self.pi*(self.diameter+self.diameter2)
                print('Площадь эллипсиса:{} , Периметр эллипса:{}'.format(ploshad_of_ellips,perimetr_of_ellips))
a=Volumetric_figure('KUB',10,10,10)
b=Flat_figure_with_top('kvadrat',15,10)
c=Flat_figure_without_top_ellips('ellips',100,50)
d=Flat_figure_without_top_circle('krug',100,50)
figurs=[a,b,c,d]
for figure in figurs:
	figure.tell()
